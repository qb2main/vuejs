<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/start', 'HomeController@start')->name('start');
Route::get('/send-data', 'HomeController@sendData')->name('send_data');
Route::get('/send-data-ajax-page', 'HomeController@sendDataAjaxPage')->name('send_data_ajax_page');
Route::get('/send-data-ajax', 'HomeController@sendDataAjax')->name('send_data_ajax');

Route::get('/chat', 'ChatController@index')->name('chat');
Route::get('/get-chat-messages', 'ChatController@getChatMessages')->name('get_chat_messages');
Route::post('/send-chat-message', 'ChatController@sendChatMessage')->name('send_chat_message');

Route::get('/get-user-id', 'ChatController@getUserId')->name('get_user_id');

Route::get('/chat-1', 'Chat1Controller@index')->name('chat_1');
Route::get('/get-chat1-messages', 'Chat1Controller@getChat1Messages')->name('get_chat_1_messages');
Route::post('/send-chat1-message', 'Chat1Controller@sendChat1Message')->name('send_chat_1_message');
