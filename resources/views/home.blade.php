@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <ul>
                        <li><a href="{{ url('start') }}">Start</a></li>
                        <li><a href="{{ url('send-data') }}">Send Data</a></li>
                        <li><a href="{{ url('send-data-ajax-page') }}">Send Data Ajax</a></li>
                        <li><a href="{{ url('chat') }}">Chat in divs</a></li>
                        <li><a href="{{ url('chat-1') }}">Chat in textarea with autoscroll</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
