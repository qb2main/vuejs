
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('example-component1', require('./components/ExampleComponent1.vue'));
Vue.component('example-component2', require('./components/ExampleComponent2.vue'));

Vue.component('send-data', require('./components/SendDataComponent.vue'));
Vue.component('send-data-ajax', require('./components/SendDataAjaxComponent.vue'));
Vue.component('chat', require('./components/ChatComponent.vue'));
Vue.component('chat1', require('./components/Chat1Component.vue'));

const app = new Vue({
    el: '#app'
});
