<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Events\SendChatMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{
    /**
     * ChatController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('chat');
    }

    /**
     * @return mixed
     */
    public function getChatMessages() {
        $show_messages_count = 100;
        $skip = ChatMessage::count()-$show_messages_count;
        $skip = $skip > 0 ? $skip : 0;
        $messages = ChatMessage::orderBy('id')->with('user')->skip($skip)->take($show_messages_count)->get();

        return $messages;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function sendChatMessage(Request $request) {

        $message = $request->message;
        $user = Auth::user();
        if ($user) {
            ChatMessage::create([
                'text' => $message,
                'user_id' => $user->id,
            ]);
            event(new SendChatMessage($user, $message));

        }

        return ['user_id' => $user->id, 'message' => $message];
    }

    /**
     * @return mixed
     */
    public function getUserId() {
        $user = Auth::user();
        if ($user) {
            return ['user_id' => $user->id];
        }
    }
}
