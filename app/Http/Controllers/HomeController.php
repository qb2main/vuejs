<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function start()
    {
        return view('start');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sendData()
    {
        $data = [
            [
                'title' => 'Some title 1',
                'url' => 'Some url 1'
            ],
            [
                'title' => 'Some title 2',
                'url' => 'Some url 2'
            ],
            [
                'title' => 'Some title 3',
                'url' => 'Some url 3'
            ],

        ];

        return view('send_data')->with('data', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sendDataAjaxPage()
    {
        return view('send_data_ajax');
    }


    /**
     * @return array (in this mode will be auto generate in JSON)
     */
    public function sendDataAjax()
    {
        return [
            [
                'title' => 'Some title 1',
                'url' => 'Some url 1'
            ],
            [
                'title' => 'Some title 2',
                'url' => 'Some url 2'
            ],
            [
                'title' => 'Some title 3',
                'url' => 'Some url 3'
            ],

        ];
    }
}
