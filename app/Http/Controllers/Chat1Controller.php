<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Events\SendChat1Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Chat1Controller extends Controller
{
    /**
     * ChatController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('chat1');
    }

    /**
     * @return mixed
     */
    public function getChat1Messages() {
        $show_messages_count = 100;
        $skip = ChatMessage::count()-$show_messages_count;
        $skip = $skip > 0 ? $skip : 0;
        $messages = ChatMessage::orderBy('id')->with('user')->skip($skip)->take($show_messages_count)->get();
        $chat = [];
        foreach ($messages as $message) {
            $chat[] = $message->user->email . ': ' . $message->text;
        }

        return $chat;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function sendChat1Message(Request $request) {

        $message = $request->message;
        $user = Auth::user();
        if ($user) {
            ChatMessage::create([
                'text' => $message,
                'user_id' => $user->id,
            ]);
            event(new SendChat1Message($user, $message));

        }

        return ['user_id' => $user->id, 'message' => $message];
    }

}
